import { Router } from 'express';
import * as firebase from 'firebase-admin';
import * as util from 'util';

const router = Router();

/**
 * 설문에 해당 이름이 사용 가능 여부
 *
 * @param surveyId 설문지 ID
 * @param username 유저 이름
 *
 * @return boolean
 */
router.post('/is-available-username', async (req, res) => {
	req.checkBody('surveyId', 'surveyId is required').notEmpty();
	req.checkBody('username', 'username is required').notEmpty();

	try {
		const result = await req.getValidationResult();

		if (!result.isEmpty()) {
			res.status(400).send('There have been validation errors: \n' + util.inspect(result.array()));
			return;
		}

		const surveyId = req.body.surveyId;
		const surveyRef = await firebase.database().ref('surveys').child(surveyId).once('value');

		if(!surveyRef.exists()) {
			res.status(400).send('This surveyId is invalid.');
			return;
		}

		const ref = await firebase.database().ref(`survey_answered/${surveyId}/${req.body.username}`).once('value');

		res.send(!ref.exists());
	} catch (e) {
		res.status(500).send(e.message);
	}
});

export const UserRouter = router;
