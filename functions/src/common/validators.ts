import { RequestHandler } from 'express';

export const validateFirebaseIdToken: RequestHandler = (req, res, next) => {
	console.log('Check if request is authorized with Firebase ID token');

	let authorization = req.headers['authorization'] as string;

	if ((!authorization || !authorization.startsWith('Bearer ')) && !req.cookies.__session) {
		console.error('No Firebase ID token was passed as a Bearer token in the Authorization header.',
			'Make sure you authorize your request by providing the following HTTP header:',
			'Authorization: Bearer <Firebase ID Token>',
			'or by passing a "__session" cookie.');
		res.status(403).send('Unauthorized');
		return;
	}

	let idToken;
	if (authorization && authorization.startsWith('Bearer ')) {
		console.log('Found "Authorization" header');
		idToken = authorization.split('Bearer ')[1];
	} else {
		console.log('Found "__session" cookie');
		idToken = req.cookies.__session;
	}
	admin.auth().verifyIdToken(idToken).then(decodedIdToken => {
		console.log('ID Token correctly decoded', decodedIdToken);
		req['user'] = decodedIdToken;
		next();
	}).catch(error => {
		console.error('Error while verifying Firebase ID token:', error);
		res.status(403).send('Unauthorized');
	});
};
