import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as express from 'express';
import expressValidator = require('express-validator');
import { UserRouter } from './user/index';


const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser')();
const cors = require('cors')({origin: true});

const app = express();

admin.initializeApp(functions.config().firebase);

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors);
app.use(cookieParser);
app.use(expressValidator());
app.get('/hello', (req, res) => {
	res.send('hello world');
});

app.use('/user', UserRouter);

export const api = functions.https.onRequest(app);
