import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-join-end',
	template: `
		<section class="hero is-light is-fullheight is-bold">
			<div class="hero-body">
				<div class="container has-text-centered">
					<h1 class="title">
						Thanks a lot!
					</h1>
					<h2 class="subtitle">
						Have a nice day.
					</h2>
				</div>
			</div>
		</section>
	`,
	styles: [`

	`]
})
export class JoinEndComponent implements OnInit {

	constructor() {
	}

	ngOnInit() {
	}
}
