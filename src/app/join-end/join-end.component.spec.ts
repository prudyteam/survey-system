import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinEndComponent } from './join-end.component';

describe('JoinEndComponent', () => {
	let component: JoinEndComponent;
	let fixture: ComponentFixture<JoinEndComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
				declarations: [JoinEndComponent]
			})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(JoinEndComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
