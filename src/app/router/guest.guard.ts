import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { SessionService } from '../shared/session.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { ToasterService } from 'angular2-toaster';

@Injectable()
export class GuestGuard implements CanActivate {
	constructor(private router: Router, private sessionService: SessionService, private toast: ToasterService) {
	}

	canActivate(next: ActivatedRouteSnapshot,
					state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
		const guest = this.sessionService.isGuest();

		guest.then(isGuest => {
			if (!isGuest) {
				this.toast.pop('warning', 'please logout', 'you try to access login page.');
				this.router.navigateByUrl('admin');
			}
		});

		return guest;
	}
}
