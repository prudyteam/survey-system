import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { SessionService } from '../shared/session.service';
import { ToasterService } from 'angular2-toaster';

@Injectable()
export class LoggedInGuard implements CanActivate {
	constructor(private router: Router, private sessionService: SessionService, private toast: ToasterService) {
	}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

		const login = this.sessionService.isLoggedIn();

		login.then(isLoggedIn => {
			if (!isLoggedIn) {
				this.toast.pop('warning', 'Invalid security', 'you need to login!');
				this.router.navigateByUrl('login');
			}
		});

		return login;
	}
}
