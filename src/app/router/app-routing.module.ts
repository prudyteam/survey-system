import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '../login/login.component';
import { LoggedInGuard } from './logged-in.guard';
import { LoadingComponent } from '../loading/loading.component';
import { GuestGuard } from './guest.guard';
import { AdminComponent } from '../admin/admin.component';
import { SurveyListComponent } from '../admin/survey-list/survey-list.component';
import { SurveyAddComponent } from '../admin/survey-add/survey-add.component';
import { JoinSurveyComponent } from '../join-survey/join-survey.component';
import { JoinEndComponent } from '../join-end/join-end.component';
import { SurveyTabsComponent } from '../admin/survey-detail/survey-tabs.component';

const routes: Routes = [
	{
		path: '',
		component: LoadingComponent,
	},
	{
		path: 'login',
		component: LoginComponent,
		canActivate: [ GuestGuard ]
	},
	{
		path: 'admin',
		component: AdminComponent,
		canActivate: [ LoggedInGuard ],
		children: [
			{
				path: '',
				component: SurveyListComponent
			},
			{
				path: 'new',
				component: SurveyAddComponent
			},
			{
				path: 'surveys/:id',
				component: SurveyTabsComponent
			}
		]
	},
	{
		path: 'join-survey/:id',
		component: JoinSurveyComponent
	},
	{
		path: 'join-end',
		component: JoinEndComponent
	}
];


@NgModule({
	imports: [ RouterModule.forRoot(routes) ],
	exports: [ RouterModule ]
})
export class AppRoutingModule {
}
