import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { EmailValidators } from 'ngx-validators';
import { ToasterService } from 'angular2-toaster';
import { SessionService } from '../shared/session.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: [ './login.component.sass' ]
})
export class LoginComponent implements OnInit {

	public email = new FormControl('', [ Validators.required, EmailValidators.normal ]);
	public password = new FormControl('', [ Validators.required, Validators.minLength(8) ]);

	public loginForm = new FormGroup({ email: this.email, password: this.password });

	constructor(private toasterService: ToasterService, private sessionService: SessionService, private router: Router) {
	}

	ngOnInit() {
	}

	async submit() {
		if (this.loginForm.valid) {
			try {
				await this.sessionService.login(this.email.value, this.password.value);
				this.router.navigate(['admin']);
			} catch (e) {
				this.toasterService.pop('warning', 'Fail to login', e.message);
			}
		} else {
			this.toasterService.pop('error', 'Check your inputs', 'Your inputs is not valid!');
		}
	}
}
