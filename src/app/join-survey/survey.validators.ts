import { AsyncValidatorFn, FormControl, ValidationErrors } from '@angular/forms';
import { Injectable } from '@angular/core';
import { SurveyService } from '../admin/shared/survey.service';
import { UtilService } from '../shared/util.service';


@Injectable()
export class SurveyValidators {

	constructor(private util: UtilService, private surveyService: SurveyService) {

	}

	/**
	 * 이메일 가용 검증
	 *
	 * @param memberService 멤버 서비스
	 * @returns {AsyncValidatorFn}
	 */
	public getAvailableUsernameRule(surveyId: string): AsyncValidatorFn {
		return this.util.asyncDebounceValidator((control: FormControl) => {

			const rule = { 'usernameAvailableRule': true };
			return this.surveyService
				.isAvailableAnswerUserName(surveyId, control.value)
				.then(availability => Promise.resolve<ValidationErrors>(availability ? null : rule))
				.catch(() => Promise.resolve<ValidationErrors>({'invalidHttpRequestRule': true}));
		});
	}
}
