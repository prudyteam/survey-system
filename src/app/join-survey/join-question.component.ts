import { Component, Input, OnInit } from '@angular/core';
import { Question } from '../admin/shared/survey';

@Component({
	selector: 'app-join-question',
	template: `
		<div class="field question">
			<div class="field head">
				<div class="control">
					<h3> {{ question.content }} </h3>
					<span style="color: tomato">*</span>
					<span *ngIf="question.type == 0">Please select Yes / No.</span>
					<span *ngIf="question.type == 1">Only one can be selected.</span>
					<span *ngIf="question.type == 2">Multiple selections are possible.</span>
				</div>
			</div>
			<div class="field is-marginless" *ngFor="let option of question.options; let qi = index">
				<p class="control">
					<label [class.radio]="question.type < 2" [class.checkbox]="question.type == 2">
						<input [type]="question.type < 2 ? 'radio' : 'checkbox'" [name]="group" (change)="updateAnswer(question, qi, $event)">
						<span>{{ option }}</span>
					</label>
				</p>
			</div>
		</div>
	`,
	styleUrls: ['./join-question.component.sass']
})
export class JoinQuestionComponent implements OnInit{
	@Input() question: Question;
	@Input() group: number;

	constructor() {

	}

	ngOnInit(): void {
	}

	updateAnswer(question: Question, index: number, event) {
		let q = question as any;

		if (!q.answer || q.type < 2) {
			q.answer = {};
		}

		q.answer[index] = event.target.checked;
		console.log(q.answer, event.target.checked);
	}
}
