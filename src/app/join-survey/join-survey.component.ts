import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { QuestionAnswer, SurveyService } from '../admin/shared/survey.service';
import 'rxjs/add/operator/switchMap';
import { Question, Survey } from '../admin/shared/survey';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SurveyValidators } from './survey.validators';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/toPromise';
import { QuestionService } from '../admin/shared/question.service';

@Component({
	selector: 'app-join-survey',
	templateUrl: './join-survey.component.html',
	styleUrls: [ './join-survey.component.sass' ]
})
export class JoinSurveyComponent implements OnInit, OnDestroy {

	public survey: Survey;
	public questions: Question[];
	public error = true;
	public visibleLoading = true;
	public subscription: Subscription;
	public username: FormControl;
	public formGroup = new FormGroup({});
	public storedUsername: string;

	constructor(private route: ActivatedRoute, private router: Router, private surveyService: SurveyService,
					private surveyValidators: SurveyValidators, private questionService: QuestionService) {
	}


	async ngOnInit() {
		const params = await this.route.params.take(1).toPromise();

		const surveyId = params[ 'id' ];

		this.formGroup.addControl(
			'username',
			this.username = new FormControl('', [ Validators.required ], [ this.surveyValidators.getAvailableUsernameRule(surveyId) ]));

		this.subscription = this.surveyService.getOne(surveyId).subscribe(survey => {
			if (survey.$exists()) {
				this.survey = survey;
				setTimeout(() => this.visibleLoading = false, 1000);
			} else {
				this.error = false;
			}
		});
	}

	ngOnDestroy(): void {
		if (this.subscription) this.subscription.unsubscribe();
	}

	async startSurvey() {
		if (this.formGroup.valid) {
			this.storedUsername = this.username.value;
			this.questions = await this.questionService.getQuestions(this.survey.questions);
		}
	}

	updateAnswer(question: Question, index: number, event) {
		const q = question as any;

		if (!q.answer || q.type < 2) {
			q.answer = {};
		}

		q.answer[ index ] = event.target.checked;
		console.log(q.answer, event.target.checked);
	}

	async finished() {

		const questions = this.questions.map((question: any) => {
			return {
				questionId: question.$key,
				optionsSnapId: question.optionsSnapId,
				select : Object.keys(question.answer).filter(key => question.answer[ key ]).map(index => +index)
			} as QuestionAnswer;
		});

		await this.surveyService.saveAnswer(this.survey.$key, this.storedUsername, questions);
		this.router.navigate([ 'join-end' ]);
	}
}
