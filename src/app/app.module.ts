import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './router/app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { SessionService } from './shared/session.service';
import { LoadingComponent } from './loading/loading.component';
import { LoggedInGuard } from './router/logged-in.guard';
import { GuestGuard } from './router/guest.guard';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToasterModule } from 'angular2-toaster';
import { AdminComponent } from './admin/admin.component';
import { SurveyListComponent } from './admin/survey-list/survey-list.component';
import { SurveyItemComponent } from './admin/survey-item/survey-item.component';
import { SurveyItemAddComponent } from './admin/survey-item-add/survey-item-add.component';
import { SurveyAddComponent } from './admin/survey-add/survey-add.component';
import { SurveyQuestionComponent } from './admin/shared/survey-question/survey-question.component';
import { DragulaModule } from 'ng2-dragula';
import { QuestionService } from './admin/shared/question.service';
import { OptionItemComponent } from './admin/shared/survey-question/option-item/option-item.component';
import { SurveyService } from './admin/shared/survey.service';
import { UtilService } from './shared/util.service';
import { KeysPipe } from './shared/keys.pipe';
import { JoinSurveyComponent } from './join-survey/join-survey.component';
import { SurveyValidators } from './join-survey/survey.validators';
import { environment } from '../environments/environment';
import { ConnectionBackend, HttpModule, XHRBackend } from '@angular/http';
import { ApiHttp } from './shared/api-http.service';
import { JoinEndComponent } from './join-end/join-end.component';
import { JoinQuestionComponent } from './join-survey/join-question.component';
import { SurveyTabsComponent } from './admin/survey-detail/survey-tabs.component';
import { OptionsSnapService } from './shared/options-snap.service';


@NgModule({
	declarations: [
		AppComponent,
		LoginComponent,
		LoadingComponent,
		AdminComponent,
		SurveyListComponent,
		SurveyItemComponent,
		SurveyItemAddComponent,
		SurveyAddComponent,
		SurveyQuestionComponent,
		OptionItemComponent,
		KeysPipe,
		JoinSurveyComponent,
		JoinEndComponent,
		JoinQuestionComponent,
		SurveyTabsComponent
	],
	imports: [
		AngularFireModule.initializeApp(environment.config),
		AngularFireDatabaseModule,
		AngularFireAuthModule,
		BrowserModule,
		ReactiveFormsModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		ToasterModule,
		DragulaModule,
		FormsModule,
		HttpModule
	],
	providers: [
		SessionService,
		LoggedInGuard,
		GuestGuard,
		QuestionService,
		SurveyService,
		UtilService,
		SurveyValidators,
		ApiHttp,
		OptionsSnapService,
		{
			provide: ConnectionBackend,
			useClass: XHRBackend
		}
	],
	bootstrap: [AppComponent]
})
export class AppModule {
}
