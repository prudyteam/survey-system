import { Component, OnInit } from '@angular/core';
import { SessionService } from '../shared/session.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-loading',
	templateUrl: './loading.component.html',
	styleUrls: [ './loading.component.sass' ]
})
export class LoadingComponent implements OnInit {

	constructor(private sessionService: SessionService, private router: Router) {
	}

	ngOnInit() {
		const login = this.sessionService.isLoggedIn();

		login.then(auth => this.router.navigate([auth ? 'admin' : 'login']))
	}
}
