import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveyTabsComponent } from './survey-tabs.component';

describe('SurveyTabsComponent', () => {
	let component: SurveyTabsComponent;
	let fixture: ComponentFixture<SurveyTabsComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
				declarations: [SurveyTabsComponent]
			})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SurveyTabsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
