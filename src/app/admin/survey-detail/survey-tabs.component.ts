import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-survey-tabs',
	templateUrl: './survey-tabs.component.html',
	styleUrls: ['./survey-tabs.component.sass']
})
export class SurveyTabsComponent implements OnInit {

	public tabs = [
		{
			name : 'Detail',
			path : 'detail'
		},
		{
			name: 'Update',
			path: 'update'
		},
		{
			name: 'Logs',
			path: 'logs'
		},
		{
			name: 'Report',
			path: 'report'
		}
	]

	constructor() {
	}

	ngOnInit() {
	}

}
