import { Component, OnInit } from '@angular/core';
import { SessionService } from '../shared/session.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-admin',
	templateUrl: './admin.component.html',
	styleUrls: [ './admin.component.sass' ]
})
export class AdminComponent implements OnInit {

	constructor(private sessionService: SessionService, private router: Router) {
	}

	ngOnInit() {
	}

	logout() {
		this.sessionService.logout();
	}

	home() {
		this.router.navigate(['/admin']);
	}
}
