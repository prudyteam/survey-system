import { Component, Input, OnInit } from '@angular/core';
import { Survey } from '../shared/survey';
import { Router } from '@angular/router';
import { SurveyService } from '../shared/survey.service';
import { UtilService } from '../../shared/util.service';
import { ToasterService } from 'angular2-toaster';
import { FirebaseKey } from '../../shared/firebase-item-meta';

@Component({
	selector: 'app-survey-item',
	templateUrl: './survey-item.component.html',
	styleUrls: [ './survey-item.component.sass' ]
})
export class SurveyItemComponent implements OnInit {

	@Input() survey: Survey;

	constructor(private router: Router, private surveySurvice: SurveyService, private util: UtilService, private toast: ToasterService) {
	}

	ngOnInit() {
		if (!this.survey) {
			this.survey = new Survey('Test Survey', 'No description');
		}
	}
	detail() {
		this.router.navigate([`admin/surveys/${this.survey.$key}`]);
	}

	copy(surveyId: FirebaseKey) {
		this.util
			.clipboardCopy(location.origin + '/join-survey/' + surveyId as string)
			.then(url => this.toast.pop('success', 'Copied to clipboard.', url));
	}

	update() {
		// Todo not yet implemented
		this.router.navigate([`admin/update/${this.survey.$key}`]);
	}

	remove() {
		console.log(this.survey);

		this.surveySurvice.remove(this.survey.$key as string);
	}

}
