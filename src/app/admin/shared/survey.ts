import { FirebaseItemMeta, FirebaseKey } from '../../shared/firebase-item-meta';

/**
 * 설문지 도메인 클래스
 */
export class Survey extends FirebaseItemMeta {
	constructor(public title: string, public description: string, public questions?: FirebaseKey[], public owner?: string) {
		super();
	}
}

/**
 * 질문
 */
export class Question extends FirebaseItemMeta {
	constructor(public content: string = '', public type: QuestionType = QuestionType.YES_NO, public options: string[] = ['', ''], public optionsSnapId?: FirebaseKey, public owner?: string) {
		super();
	}
}

export class OptionsSnap extends FirebaseItemMeta {
	constructor(public options: string[] = [], public ts?: any) {
		super();
	}
}

/**
 * 설문지 타입
 */
export enum QuestionType { YES_NO, SINGLE, MULTIPLE }
