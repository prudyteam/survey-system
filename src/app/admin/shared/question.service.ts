import { Injectable } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseKey } from '../../shared/firebase-item-meta';
import { UtilService } from '../../shared/util.service';
import { OptionsSnap, Question, QuestionType } from './survey';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/toPromise';
import { OptionsSnapService } from '../../shared/options-snap.service';
import { SessionService } from '../../shared/session.service';


@Injectable()
export class QuestionService {

	private questionRef = this.database.list('questions');

	constructor(private fb: FormBuilder, private database: AngularFireDatabase, private util: UtilService,
					private optionService: OptionsSnapService, private sessionService: SessionService) {
	}

	/**
	 * 질문 생성
	 *
	 * @param question
	 * @returns {Promise<firebase.database.ThenableReference>}
	 */
	public async add(question: Question): Promise<Question> {
		question = this.util.create<Question>(question, Question);

		if (question.type == QuestionType.YES_NO && question.options.length > 2) {
			question.options = question.options.slice(0, 2);
		}

		const optionRef = await this.optionService.add(new OptionsSnap(question.options));

		question.owner = await this.sessionService.getUid();
		question.optionsSnapId = optionRef.key;

		console.log(question);
		const ref = await this.questionRef.push(question.removeMeta());

		question.$key = ref.key;

		return question;
	}

	/**
	 * 폼 생성
	 *
	 * @param question
	 * @returns {FormGroup}
	 */
	public createForm(question: Question = new Question()) {
		return this.fb.group({
			content: [ question.content, Validators.required ],
			type: [ question.type, Validators.required ],
			options: this.fb.array(question.options.map(option => [ option, Validators.required ]), Validators.min(2))
		});
	}

	/**
	 * 질문 조회
	 *
	 * @param questionKeys
	 * @returns {Promise<[Question,T2,T3,T4,T5,T6,T7,T8,T9,T10]>}
	 */
	public async getQuestions(questionKeys: FirebaseKey[]): Promise<Question[]> {
		const $questions: Promise<Question>[] = questionKeys.map(key =>
			this.database.object(`/questions/${key}`)
				.map(data => this.convert(data)).take(1)
				.toPromise());

		return await Promise.all($questions);
	}

	/**
	 * 데이터에서 Qusetion 객체로 변환
	 *
	 * @param data
	 * @returns {Question}
	 */
	private convert(data: any): Question {
		return this.util.create<Question>(data, Question);
	}
}
