import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { UtilService } from '../../shared/util.service';
import { Question, Survey } from './survey';
import 'rxjs/add/operator/map';
import { FirebaseApp } from 'angularfire2';
import { SessionService } from '../../shared/session.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/take';
import { ApiHttp } from '../../shared/api-http.service';
import { QuestionService } from './question.service';
import { FirebaseKey } from '../../shared/firebase-item-meta';
import * as firebase from 'firebase';

@Injectable()
export class SurveyService {

	constructor(public database: AngularFireDatabase, public util: UtilService, public app: FirebaseApp,
					public sessionService: SessionService, public apiHttp: ApiHttp, public questionService: QuestionService) {
	}


	/**
	 * 설문 단건 조회
	 *
	 * @param surveyId
	 * @returns {Observable<R>}
	 */
	public getOne(surveyId: string) {
		return this.database.object(`surveys/${surveyId}`).map(data => this.dataToSurvey(data));
	}

	/**
	 * 설문 다건 조회
	 *
	 * @returns {Observable<R>}
	 */
	public async getSurveys() {
		const uid = await this.sessionService.getUid();

		return this.database.list(`surveys`, {
			query: {
				orderByChild: 'owner',
				equalTo: uid
			}
		}).map((datas: any[]) => {
			console.log(datas);
			return datas.map(data => this.dataToSurvey(data));
		});
	}


	/**
	 * 설문 저장
	 *
	 * @param {Survey} survey
	 * @return Promise<FirebaseKey>
	 */
	public async add(survey: any): Promise<FirebaseKey> {
		const uid = await this.sessionService.getUid();

		const questions = survey.questions as Question[];
		const questionRefs = await Promise.all(questions.map(q => this.questionService.add(q)));

		console.log(questionRefs.map(ref => ref.$key));


		survey.owner = uid;
		survey.questions = questionRefs.map(ref => ref.$key);

		const surveyRef = this.database.list(`surveys`).push(survey);

		return surveyRef.key;
	}

	public dataToSurvey(data: any) {
		return this.util.create<Survey>(data, Survey);
	}

	/**
	 *
	 * @param {string} surveyId
	 * @returns {firebase.Promise<any>}
	 */
	public remove(surveyId: string) {
		return this.app.database()
			.ref(`surveys`)
			.child(surveyId)
			.remove();
	}

	/**
	 * 해당 유저 이름이 사용 가능 여부 반환
	 *
	 * @param {string} surveyId
	 * @param {string} username
	 * @returns {Promise<boolean>}
	 */
	public async isAvailableAnswerUserName(surveyId: string, username: string) {
		const result = await this.apiHttp.post('/user/is-available-username', { surveyId, username }).toPromise();

		return result.json();
	}


	public saveAnswer(surveyId, username, questions: QuestionAnswer[]) {
		const result: SurveyAnswer = { questions, ts: firebase.database.ServerValue.TIMESTAMP };

		return this.database.object(`survey_answered/${surveyId}/${username}`).set(result);
	}
}


export interface QuestionAnswer {
	questionId: FirebaseKey;
	optionsSnapId: FirebaseKey;
	select: number[];
}

export interface SurveyAnswer {
	ts: Date | Object;
	questions: QuestionAnswer[];
}
