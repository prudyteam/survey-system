import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
	selector: 'app-option-item',
	template: `
		<p class="control" *ngIf="label">
			<a class="button is-static">
				{{label}}
			</a>
		</p>
		<p class="control is-expanded" [class.is-expaned]="enableRemove">
			<input class="input" placeholder="Input your option" [formControl]="option"
					 [ngClass]="{'is-danger': option.dirty && option.hasError('required'), 'is-success': option.valid}">
		</p>
		<p class="control" style="margin-right: 0" *ngIf="!enableRemove">
			<a class="button" (click)="remove.emit(index)"><span class="icon is-small"><i class="fa fa-times"></i></span></a>
		</p>
	`
})
export class OptionItemComponent implements OnInit {

	@Input() public option: FormControl;
	@Input() public index: number;
	@Input() public enableRemove: boolean = true;
	@Input() public label: string;
	@Output() public remove = new EventEmitter<number>();

	constructor() {
	}

	ngOnInit() {
	}

}
