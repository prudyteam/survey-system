import { Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToasterService } from 'angular2-toaster';
import { OptionItemComponent } from './option-item/option-item.component';
import { QuestionType } from '../survey';

@Component({
	selector: 'app-survey-question',
	templateUrl: './survey-question.component.html',
	styleUrls: ['./survey-question.component.sass']
})
export class SurveyQuestionComponent implements OnInit {

	@Input() public questionForm: FormGroup;

	@ViewChildren(OptionItemComponent) public optionCmps: QueryList<OptionItemComponent>;

	@Output() public close = new EventEmitter<void>();

	public types = QuestionType;

	constructor(public fb: FormBuilder, private toast: ToasterService) {
	}

	ngOnInit() {
		if (!this.questionForm) {
			this.questionForm = this.fb.group({
				content: ['', Validators.required],
				type: [QuestionType.MULTIPLE, Validators.required]
			});
			this.setOptions(['', '']);
		}

	}

	get content() {
		return this.questionForm.controls['content'];
	}

	get type() {
		return this.questionForm.controls['type'];
	}

	setOptions(options: string[]) {
		this.questionForm.setControl('options', this.fb.array(options.map(option => this.fb.control(option, Validators.required))));
	}

	get options() {
		return this.questionForm.get('options') as FormArray;
	}

	newOption() {
		this.options.push(this.fb.control('', Validators.required));
	}

	removeOption(index: number, toast: boolean = true) {
		if (this.options.length > 2) {
			this.options.removeAt(index);
		} else if (toast) {
			this.toast.pop('warning', 'Fail to remove', 'At least two is required.');
		}
	}

	cleanOptions() {
		this.options.controls
			.map((ctrl, index) => {
				return { ctrl, index };
			})
			.reverse()
			.filter(data => data.ctrl.value === '')
			.map(data => data.index)
			.forEach(index => this.removeOption(index, false));
	}
}
