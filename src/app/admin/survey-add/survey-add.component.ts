import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Question, Survey } from '../shared/survey';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DragulaService } from 'ng2-dragula';
import { QuestionService } from '../shared/question.service';
import { ToasterService } from 'angular2-toaster';
import { SurveyQuestionComponent } from '../shared/survey-question/survey-question.component';
import { SurveyService } from '../shared/survey.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-survey-add',
	templateUrl: './survey-add.component.html',
	styleUrls: ['./survey-add.component.sass']
})
export class SurveyAddComponent implements OnInit {

	@ViewChildren(SurveyQuestionComponent) questionCmps: QueryList<SurveyQuestionComponent>;

	public surveyForm: FormGroup;

	constructor(private fb: FormBuilder, private dragulaService: DragulaService,
					private questionService: QuestionService, private toast: ToasterService, private surveyService: SurveyService,
					private router: Router) {
	}

	async ngOnInit() {
		this.createForm();
		this.newQuestion();
		this.dragulaService.setOptions('question-bag', {
			moves: (el, container, handle) => {
				const className: string = handle.className;
				return className.indexOf('drag-button') !== -1;
			}
		});
	}

	createForm() {
		this.surveyForm = this.fb.group({
			title: ['', Validators.required],
			description: [''],
			questions: this.fb.array([])
		});
	}

	setQuestions(questions: Question[]) {
		const questionFGs = questions.map(question => this.fb.group(question));
		const questionFormArray = this.fb.array(questionFGs);
		this.surveyForm.setControl('questions', questionFormArray);
	}

	removeQuestion(index: number) {
		if (this.questions.length > 1) {
			this.questions.removeAt(index);
		} else {
			this.toast.pop('warning', 'Could not remove question.', 'You must have at least one question.')
		}
	}

	get questions(): FormArray {
		return this.surveyForm.get('questions') as FormArray;
	}

	get title(): FormControl {
		return this.surveyForm.get('title') as FormControl;
	}

	get description(): FormControl {
		return this.surveyForm.get('description') as FormControl;
	}

	newQuestion() {
		this.questions.push(this.questionService.createForm()); // test
	}

	private cleanEmptyOptions() {
		this.questionCmps.forEach(cmp => cmp.cleanOptions());
	}

	async save() {
		this.cleanEmptyOptions();
		const surveyRowData = this.surveyForm.getRawValue();

		if (this.surveyForm.valid) {

			try {
				const result = await this.surveyService.add(surveyRowData)
				this.toast.pop('success', 'Success to save!', `your ${surveyRowData.title} is saved!`);
				this.router.navigate(['admin']);
			} catch (e) {
				this.toast.pop('error', 'Fail to save', e.message);
			}

		} else {
			this.toast.pop('warning', 'Fail to save', 'Please check out invalid inputs.')
		}
	}
}
