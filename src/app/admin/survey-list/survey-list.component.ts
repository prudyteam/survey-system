import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Survey } from '../shared/survey';
import { SurveyService } from '../shared/survey.service';

@Component({
	selector: 'app-survey-list',
	templateUrl: './survey-list.component.html',
	styleUrls: [ './survey-list.component.sass' ]
})
export class SurveyListComponent implements OnInit {

	public surveys: Survey[];

	constructor(private router: Router, private serveySurvey: SurveyService) {
	}

	async ngOnInit() {
		const $survey = await this.serveySurvey.getSurveys();

		$survey.subscribe(surveys => this.surveys = surveys);
	}

	newSurvey() {
		this.router.navigate(['admin/new']);
	}
}
