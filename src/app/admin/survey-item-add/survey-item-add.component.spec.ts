import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveyItemAddComponent } from './survey-item-add.component';

describe('SurveyItemAddComponent', () => {
	let component: SurveyItemAddComponent;
	let fixture: ComponentFixture<SurveyItemAddComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
				declarations: [ SurveyItemAddComponent ]
			})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SurveyItemAddComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
