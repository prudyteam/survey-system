import { Component, OnInit } from '@angular/core';
import { SessionService } from './shared/session.service';
import { ToasterConfig } from 'angular2-toaster';

@Component({
	selector: 'app-root',
	templateUrl: 'app.component.html',
	styles: []
})
export class AppComponent implements OnInit {
	public toasterconfig: ToasterConfig =
		new ToasterConfig({
			showCloseButton: true,
			tapToDismiss: false,
			timeout: 5000,
			animation: 'fade',
			positionClass: 'toast-bottom-center'
		});

	constructor(private sessionService: SessionService) {
	}

	ngOnInit(): void {
		this.sessionService.service();
	}
}
