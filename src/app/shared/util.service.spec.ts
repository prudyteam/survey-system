import { inject, TestBed } from '@angular/core/testing';

import { UtilService } from './util.service';
import { FirebaseItemMeta } from './firebase-item-meta';

class TestClass extends FirebaseItemMeta {
	public testMethod() {
		return true;
	}
}

describe('UtilService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [ UtilService ]
		});
	});

	it('should be created', inject([ UtilService ], (service: UtilService) => {
		expect(service).toBeTruthy();
	}));

	it('should be defined prototype on object', inject([ UtilService ], (service: UtilService) => {

		const test = service.create<TestClass>({}, TestClass);

		expect(test.testMethod()).toBe(true);
	}));
});
