import { Injectable } from '@angular/core';
import { BaseRequestOptions, ConnectionBackend, Headers, Http, RequestOptions, RequestOptionsArgs } from '@angular/http';
import { environment } from '../../environments/environment';

@Injectable()
export class ApiHttp extends Http {

	constructor(backend: ConnectionBackend) {
		super(backend, new BaseCommonRequestOptions());
	}

}

export class BaseCommonRequestOptions extends BaseRequestOptions {

	merge(options?: RequestOptionsArgs): RequestOptions {
		return new CommonRequestOptions(super.merge(extracted(options)));
	}
}

/**
 * for inner merge when using post put patch delete...others method
 */
export class CommonRequestOptions extends RequestOptions {
	merge(options?: RequestOptionsArgs): RequestOptions {
		return new RequestOptions(super.merge(extracted(options)));
	}
}

/**
 * inject default values
 *
 * @param options
 * @returns {RequestOptionsArgs}
 */
export function extracted(options: RequestOptionsArgs) {
	if (!validUrl(options.url)) {
		options.url = environment.apiBaseUrl + (options.url ? options.url : '');
	}

	// use default header application/json, if content-type header was empty.
	if (options.headers != null) {
		const contentType = options.headers.get('content-type');
		if (contentType == null || contentType === '') {
			options.headers.append('content-type', 'application/json');
		}
	} else {
		options.headers = new Headers({ 'content-type': 'application/json' });
	}

	return options;
}

/**
 * validate url
 *
 * @param url
 * @returns {boolean}
 */
export function validUrl(url: string) {
	return /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/.test(url);
}
