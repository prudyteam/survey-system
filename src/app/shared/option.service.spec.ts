import { inject, TestBed } from '@angular/core/testing';

import { OptionsSnapService } from './options-snap.service';

describe('OptionsSnapService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [ OptionsSnapService ]
		});
	});

	it('should be created', inject([ OptionsSnapService ], (service: OptionsSnapService) => {
		expect(service).toBeTruthy();
	}));
});
