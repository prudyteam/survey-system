/**
 * 엔티티로써 저장하는 값 객체 공통 클래스
 */
export abstract class FirebaseItemMeta {
	constructor(public $key?: FirebaseKey, public $exists?: () => boolean) {

	}

	/**
	 * Firebase에서 제공하는 메타성 필드를 삭제
	 *
	 * @returns {{}&FirebaseItemMeta}
	 */
	public removeMeta() {
		const clone = Object.assign({}, this);

		Object.keys(clone)
			.filter(key => key.charAt(0) === '$' || clone[key] === undefined)
			.forEach(key => {
				delete clone[ key ];
			});

		return clone;
	}
}


export declare type FirebaseKey = string | number | boolean;
