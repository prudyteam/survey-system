import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { UtilService } from './util.service';
import 'rxjs/add/operator/map';
import { User } from './user';

@Injectable()
export class UserService {

	constructor(public database: AngularFireDatabase, public util: UtilService) {
	}

	/**
	 * 사용자 단건 조회
	 *
	 * @param userUid
	 * @returns {FirebaseObjectObservable<any>}
	 */
	public getOne(userUid: string) {
		return this.database.object(`users/${userUid}`).map(this.dataToUser);
	}

	/**
	 * 사용자 다건 조회
	 *
	 * @returns {FirebaseListObservable<any[]>}
	 */
	public getUsers() {
		return this.database.list(`users`).map(this.dataToUser);
	}


	/**
	 * 값 객체에 prototype 주입
	 *
	 * @description Firebase에서 넘어온 모든 객체들은 prototype이 없는 값 객체으므로 멤버 메서드를 쓸때는 반드시 prototype을 세팅해줘야 한다.
	 *
	 * @param data
	 * @returns {User}
	 */
	private dataToUser(data: any) {
		return this.util.create<User>(data, User);
	}
}
