import { TestBed, inject } from '@angular/core/testing';

import { ApiHttp } from './api-http.service';

describe('ApiHttp', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApiHttp]
    });
  });

  it('should be created', inject([ApiHttp], (service: ApiHttp) => {
    expect(service).toBeTruthy();
  }));
});
