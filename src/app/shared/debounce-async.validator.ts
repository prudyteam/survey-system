import { AsyncValidatorFn } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';

/**
 * 비동기 밸리데이터를 위한 Debounce 헬퍼
 */
export class DebounceAsyncValidator {

	private validate;

	constructor(validator: AsyncValidatorFn, debounceTime = 1000) {
		const source = new Observable<any>(subscriber => {
			this.validate = (control) => subscriber.next(control);
		});

		source.debounceTime(debounceTime)
			.map(x => {
				return { promise: validator(x.control), resolver: x.promiseResolver };
			})
			.subscribe(x => {
				if (x.promise instanceof Observable) x.promise = x.promise.toPromise();
				x.promise.then(x.resolver);
			});
	}

	/**
	 * debounce로 Wrapping된   AsyncValidator를 반환
	 *
	 * @returns {(control:any)=>Promise<T>}
	 */
	getValidator(): AsyncValidatorFn {
		return (control) => {
			let promiseResolver;

			const promise = new Promise(resolve => {
				promiseResolver = resolve;
			});
			this.validate({ control: control, promiseResolver: promiseResolver });

			return promise;
		};
	}
}
