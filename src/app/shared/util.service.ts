import { Inject, Injectable } from '@angular/core';
import { FirebaseItemMeta } from './firebase-item-meta';
import { DebounceAsyncValidator } from './debounce-async.validator';
import { AsyncValidatorFn } from '@angular/forms';
import { DOCUMENT } from '@angular/platform-browser';

/**
 * Firebase관련 데이터를 다룰 때 발생할 수 있는 공통 메서드 집합
 */
@Injectable()
export class UtilService {

	constructor(@Inject(DOCUMENT) private dom: Document) {
	}

	/**
	 * 전달된 타입으로 프로토타입을 포함하여 캐스팅 함
	 *
	 * @param object 대상 객체
	 * @param type 대상 타입
	 * @returns {T} 대상 타입
	 *
	 * @example
	 * let user = this.util.create<User>(data, User);
	 */
	public create<T extends FirebaseItemMeta>(object: any, type: Function): T {
		const clone = Object.assign({}, object);

		Object.getOwnPropertyNames(object)
			.filter(name => name.charAt(0) === '$')
			.map(name => {
				return { name: name, descriptor: Object.getOwnPropertyDescriptor(object, name) };
			})
			.forEach(prop => Object.defineProperty(clone, prop.name, prop.descriptor));

		Object.setPrototypeOf(clone, type.prototype);
		return clone as T;
	}

	/**
	 * debounce로 Wrapping된   AsyncValidator를 반환
	 *
	 * @param validator
	 * @param debounceTime
	 * @returns {AsyncValidatorFn}
	 */
	public asyncDebounceValidator(validator: AsyncValidatorFn, debounceTime = 400): AsyncValidatorFn {
		let asyncValidator = new DebounceAsyncValidator(validator, debounceTime);
		return asyncValidator.getValidator();
	}


	/**
	 * 클립보드 카피
	 *
	 * @param {string} value
	 * @return {Promise<T>}
	 */
	public clipboardCopy(value: string): Promise<string> {
		var promise = new Promise(
			(resolve, reject): void => {
				var textarea = null;
				try {
					// NOTE: This Textarea is being rendered off-screen.
					textarea = this.dom.createElement('textarea');
					textarea.style.height = '0px';
					textarea.style.left = '-100px';
					textarea.style.opacity = '0';
					textarea.style.position = 'fixed';
					textarea.style.top = '-100px';
					textarea.style.width = '0px';
					this.dom.body.appendChild(textarea);

					textarea.value = value;
					textarea.select();
					this.dom.execCommand('copy');

					resolve(value);
				} finally {
					if (textarea && textarea.parentNode) {
						textarea.parentNode.removeChild(textarea);
					}
				}
			}
		);
		return ( promise );
	}
}
