import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { OptionsSnap } from '../admin/shared/survey';
import * as firebase from 'firebase';


@Injectable()
export class OptionsSnapService {

	private optionsRef = this.database.list('options');

	constructor(private database: AngularFireDatabase) {
	}

	public add(options: OptionsSnap) {
		console.log(options);

		options.ts = firebase.database.ServerValue.TIMESTAMP;
		return this.optionsRef.push(options.removeMeta());
	}

}
