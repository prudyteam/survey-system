import { KeysPipe } from './keys.pipe';

describe('KeysPipe', () => {
	it('create an instance', () => {
		const pipe = new KeysPipe();
		expect(pipe).toBeTruthy();
	});

	it('should be returned only number key object', () => {
		const pipe = new KeysPipe();

		const result = pipe.transform({
			'1': true,
			'fail': false
		});

		expect(result[0].key).toBe('1');
		expect(result[0].value).toBe(true);
		expect(result.length).toBe(1);
	});
});
