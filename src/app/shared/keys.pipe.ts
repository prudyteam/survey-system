import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'keys'
})
export class KeysPipe implements PipeTransform {

	transform(value: any, args?: any): any {
		const result = Object.keys(value)
			.filter(key => !isNaN(parseInt(key, 10)))
			.map(key => {
				return { key, value: value[key] };
			});
		return result;
	}
}
