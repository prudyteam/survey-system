import { FirebaseItemMeta } from './firebase-item-meta';
export class User extends FirebaseItemMeta {
	constructor(public email: string) {
		super();
	}
}
