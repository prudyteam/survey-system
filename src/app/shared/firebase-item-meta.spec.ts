import { FirebaseItemMeta } from './firebase-item-meta';

class TestClass extends FirebaseItemMeta {
	constructor(key: string) {
		super(key);
	}
}

describe('FirebaseItemMeta', () => {

	it('should be created', () => {
		expect(new TestClass('test_key').$key).toBeTruthy();
	});

	it('should be removed meta fields', () => {
		const test = new TestClass('test_key').removeMeta();

		expect(test.$key).toBeUndefined();
		expect(this.$exists).toBeUndefined();
	});
});
