import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Router } from '@angular/router';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/toPromise';
import { FirebaseApp } from 'angularfire2';

@Injectable()
export class SessionService {
	private session;
	private _isApploaded: boolean;

	constructor(private auth: AngularFireAuth,
					private database: AngularFireDatabase,
					private app: FirebaseApp,
					private router: Router) {
	}

	/**
	 * 최초 앱 실행시 발행해야하는 라우팅관련 서비스
	 */
	public service() {
		this.auth.authState
			.subscribe(auth => {
				this.session = auth;
				this._isApploaded = true;
			});
	}


	public async getUser() {
		return await this.auth.authState.take(1).toPromise();
	}

	public async getUid() {
		return (await this.getUser()).uid;
	}

	/**
	 * 로그인 여부 반환
	 *
	 * @returns {Promise<T>}
	 */
	public isLoggedIn(): Promise<boolean> {
		return this.auth.authState.map(auth => auth != null).first().toPromise();
	}

	/**
	 * 게스트 여부 반환
	 *
	 * @returns {Promise<T>}
	 */
	public isGuest(): Promise<boolean> {
		return this.auth.authState.map(auth => auth == null).first().toPromise();
	}

	/**
	 * 앱 로딩 여부 반환
	 *
	 * @returns {boolean}
	 */
	public isAppLoaded() {
		return this._isApploaded;
	}

	/**
	 * 이메일과 비밀번호로 로그인
	 *
	 * @param email
	 * @param password
	 * @returns {Promise<any>}
	 */
	public async login(email: string, password: string) {
		return await this.app.auth().signInWithEmailAndPassword(email, password);
	}


	/**
	 * 로그아웃
	 *
	 * @returns {Promise<void>}
	 */
	public async logout() {
		console.log('request logout');

		this.app.database().goOffline();
		await this.app.auth().signOut();
		this.app.database().goOnline();
	}
}
