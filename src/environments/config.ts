import { FirebaseAppConfig } from 'angularfire2';

export interface AppConfig {
	hmr: boolean;
	production: boolean;
	config: FirebaseAppConfig;
	apiBaseUrl: string;
}
