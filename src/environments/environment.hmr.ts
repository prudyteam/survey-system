import { AppConfig } from './config';

import { config, localAPI } from './firebase/dev'

export const environment: AppConfig = {
	production: false,
	hmr: true,
	config,
	apiBaseUrl: localAPI
}
