import { AppConfig } from './config';
import { config, remoteAPI } from './firebase/stage'

export const environment: AppConfig = {
	production: true,
	hmr: false,
	config,
	apiBaseUrl: remoteAPI
};
