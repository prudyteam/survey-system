export const config = {
	apiKey: 'AIzaSyDsbzHWFXD3le-4piDesLJGktonKPh8Du0',
	authDomain: 'survey-system-34d33.firebaseapp.com',
	databaseURL: 'https://survey-system-34d33.firebaseio.com',
	projectId: 'survey-system-34d33',
	storageBucket: 'survey-system-34d33.appspot.com',
	messagingSenderId: '243212306003'
};

export const localAPI = 'http://localhost:5002/survey-system-34d33/us-central1/api';
export const remoteAPI = 'https://us-central1-survey-system-34d33.cloudfunctions.net/api';
