export const config = {
	'apiKey': 'AIzaSyBrHxR8JwNfrqxTM8n72z7Lrs4gms1PJIA',
	'authDomain': 'survey-system-dev.firebaseapp.com',
	'databaseURL': 'https://survey-system-dev.firebaseio.com',
	'projectId': 'survey-system-dev',
	'storageBucket': '',
	'messagingSenderId': '805875461468'
};

export const localAPI = 'http://localhost:5002/survey-system-dev/us-central1/api';
export const remoteAPI = 'https://us-central1-survey-system-dev.cloudfunctions.net/api';
