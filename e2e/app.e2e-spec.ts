import { SurveySystemPage } from './app.po';

describe('survey-system App', () => {
  let page: SurveySystemPage;

  beforeEach(() => {
    page = new SurveySystemPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
